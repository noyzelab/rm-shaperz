# RM SHAPERZ

Ring mod / wave shaper style card for the TipTop Z-DSP synth module. Coded 2017 - 2019 in spincad with some minor assembler tweaks afterwards, mainly for level/offsetting. Each algorithm has 3 pots that are acting as vca controls for L & R input level [pots 0 &2], and pot 1 is output level for both left and right channels. The feedback controls are also very useful in these algorithms for further altering sound/waveshape. 

Generally I have been driving this from a pair of vco outputs and is very good at adding harmonics in the [so called] east coast style. I got the idea for having vca on the inputs from the Serge ring mod, which has vc for the X & Y input levels, although not the output. I got that from my Hinton Music Lab, although that also has centre zero pots for levels/gain; anticlockwise is inverting input gain, and clockwise is non-inverting [something I have been thinking of maybe trying]. Of course the programs can be used for drums, vocals etc.. :]

Program list [these are also included in the display text file] ==>
[note L =  the left output signal]

0: X*Y | X+Y          
standard ringmod multiplier and 2 channel additive mixer

1: <explog> X*Y | X/Y      
multiplication and division using exponential and log functions

2: ABS[X]*ABS[Y] | L*X*Y    
absolute value shaper / ringmod

3: ABS[X]-Y | L*X      
absolute value shaper / ringmod algorithm 2
  
4: ABS[X]-ABS[Y] | L*X*Y     
absolute value shaper / ringmod algorithm 3

5: MAX[X,Y] | L*Y      
maximum value between X and Y shaper / ringmod
 
6: HRCT[Y]-X | L*Y  
half wave rectifier shaper /   ringmod

7: HRCT[X]-HRCT[Y]| L*X*Y 
half wave rectifier shaper /   ringmod algorithm 2


if u find these progz useful please think about supporting my work either thru my bandcamp page:
https://noyzelab.bandcamp.com/
or thru paypal or get in touch via noyzelab [at] gmail [dot] com

if u program up blank cards with a view to selling them to people, likewise please consider getting in touch, sending me something or contributing to my research / time costs in some way & givin me a shout :]

thanks, dave!
